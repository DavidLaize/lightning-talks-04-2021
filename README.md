# Meetup Software Crafts·wo·manship Rennes
# Lightning talks - 13 avril 2021

## Liste des talks proposés :

* **[Titre du talk]** - [Speaker(s)] - [Description rapide]

* **Des conteneurs dans mon navigateur** - [Benoît Masson](https://www.linkedin.com/in/benoitmasson/) (OVHcloud) - Présentation et démo de l'extension [multi-account containers](https://addons.mozilla.org/fr/firefox/addon/multi-account-containers/) de Firefox : cloisonnement des données, regroupement et masquage d'onglets, etc.
